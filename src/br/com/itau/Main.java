package br.com.itau;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o capital a ser investido:");
        double capitalScanner = scanner.nextDouble();

        System.out.println("Informe a quantidade de meses desejada:");
        int periodoScanner = scanner.nextInt();

        scanner.close();

        double montante = SimulaInvestimento.calculaMontante(capitalScanner, periodoScanner, 0.007);

        Impressora.imprimir(montante);
    }
}