package br.com.itau;

public class SimulaInvestimento {
    private double capital;
    private int periodo;
    private double taxa;

    public SimulaInvestimento (double capital, int periodo, double taxa){
        this.capital = capital;
        this.periodo = periodo;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    public static double calculaMontante(double capital, int periodo, double taxa){
        double montante = capital*(Math.pow((1 + taxa), periodo));
        return montante;
    }
}
